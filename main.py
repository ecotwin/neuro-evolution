import argparse
import concurrent.futures
from config import Config
from lib.runner import Runner
from lib.agent import load_agent, Agent
import numpy as np
import time


def parse_args():
    parser = argparse.ArgumentParser(description='Ecotwin neuro')
    # Config
    parser.add_argument('-c', '--config', dest='config', required=False, help='config file to use')
    parser.add_argument('-f', '--file', dest='file', required=False, help='load weights/biases from file')

    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    config: Config = Config('settings.config')
    saved_agent = None
    if args.config:
        config = Config(args.config)
    if args.file:
        folder, file = args.file.split(' ')
        saved_agent = load_agent(folder, file)

    runner = Runner(config, should_save_progress=saved_agent is None, is_root=True)

    if saved_agent:
        agents = []
        for i in range(100):
            agent = Agent(eval_mode=True)
            agent.network.weights = saved_agent.network.weights
            agent.network.biases = saved_agent.network.biases
            agents.append(agent)
        runner.load_agents(agents)
                

    # benchmark
    # start_time_simulation = time.time()
    # runner.run(False, fixed_frames=1000)
    # end_time_simulation = time.time()
    # print(f"Simulation: {end_time_simulation - start_time_simulation} seconds")
    
    for i in range(1000000):
        start_time_simulation = time.time()  # Record the start time
        if saved_agent:
            # displaying
            # runner.run(True, fixed_frames=501)
            runner.run(True, fixed_frames=5000)
        else:
            # training
            runner.multiple_runs(25, 8000)
        end_time_simulation = time.time()
        start_time_generation = time.time()
        runner.next_generation()
        end_time_generation = time.time()
        print(f"Simulation: {end_time_simulation - start_time_simulation} seconds")
        print(f"Generation: {end_time_generation - start_time_generation} seconds")
        print(f"Total time: {end_time_generation - start_time_simulation} seconds")
        print('______________________')
    
