from typing import List, Any, Tuple
from enum import Enum
import pygame # type: ignore
import numpy as np
from lib.world import terrain_to_color, Cell
from lib.agent import Agent
import math

WIDTH = 800
HEIGHT = 800
CELL_SIZE = math.floor(WIDTH / 100)
HEALTHBAR_WIDTH = CELL_SIZE * 1.5

def init_pygame():
    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Grid World Visualization")
    return screen

def color_for_distance(distance: float, max_distance: float) -> Tuple[int, int, int]:
    # if (distance == 0):
    #     return (255, 0, 0)
    # if (distance <= 3):
    #     return (255, 255, 0)

    # Normalize the distance to be between 0 and 1
    normalized_distance = distance / max_distance
    # print(distance)
    # Convert the normalized distance to a color value between 0 and 255
    color_value = int(normalized_distance * 255)
    # Return the color as a tuple
    return (color_value, color_value, color_value)

def draw_grid(screen, grid: List[List[Cell]]):
    screen.fill((255, 255, 255))  # Fill the screen with white
    max_distance = max([max([cell.distance_to_water for cell in row]) for row in grid])
    for x, row in enumerate(grid):
        for y, cell in enumerate(row):
            color = terrain_to_color[cell.terrain]
            # color = color_for_distance(cell.distance_to_water, max_distance)
            pygame.draw.rect(screen, color, (x * CELL_SIZE, y * CELL_SIZE, CELL_SIZE, CELL_SIZE))
            # write the distance to water on the cell
            # font = pygame.font.Font(None, 20)
            # text = font.render(str(cell.distance_to_water), True, (255, 255, 255))
            # screen.blit(text, (x * CELL_SIZE, y * CELL_SIZE))

def draw_texts(screen, frames, num_alive):
    font = pygame.font.Font(None, 36)
    text = font.render(f"Frames: {frames}", True, (255, 255, 255))
    screen.blit(text, (10, 10))

    text = font.render(f"Alive: {num_alive}", True, (255, 255, 255))
    screen.blit(text, (10, 50))

def rotate_point(point, angle, origin):
    """
    Rotate a point around an origin by angle.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy

def draw_arrow(screen, color, start, direction, size):
    """
    Draw a rotated arrow on the screen.
    """
    # Calculate the angle in radians
    angle = math.atan2(direction[0], direction[1])

    # Define the base arrow shape
    arrow = [
        (0, 0),           # Arrow point
        (-size // 2, -size),  # Left base
        (size // 2, -size)    # Right base
    ]

    # Rotate the arrow points
    rotated_arrow = [rotate_point(point, -angle, (0, 0)) for point in arrow]

    # Translate the arrow to the start position
    translated_arrow = [(start[0] + point[0], start[1] + point[1]) for point in rotated_arrow]

    # Draw the arrow
    pygame.draw.polygon(screen, color, translated_arrow)

def draw_agent(screen, agent: Agent, water_dir, food_dir, obs: np.ndarray):
    if not agent.is_alive:
        return

    pygame.draw.rect(screen, (255, 255, 255), (agent.pos_x * CELL_SIZE, agent.pos_y * CELL_SIZE, CELL_SIZE, CELL_SIZE))

    # Health bar position calculations
    hungerbar_x = agent.pos_x * CELL_SIZE - (HEALTHBAR_WIDTH - CELL_SIZE) / 2
    hungerbar_y = agent.pos_y * CELL_SIZE - CELL_SIZE  # Position above the agent

    # Draw the red (empty) health bar
    # pygame.draw.rect(screen, (255, 0, 0), (hungerbar_x, hungerbar_y, HEALTHBAR_WIDTH, CELL_SIZE / 4))
    pygame.draw.rect(screen, (255, 0, 0), (hungerbar_x, hungerbar_y, HEALTHBAR_WIDTH * (agent.hunger / 100), CELL_SIZE / 4))

    # Health bar position calculations
    thirstbar_x = agent.pos_x * CELL_SIZE - (HEALTHBAR_WIDTH - CELL_SIZE) / 2
    thirstbar_y = agent.pos_y * CELL_SIZE - CELL_SIZE * 1.5  # Position above the agent

    # Draw the red (empty) health bar
    # pygame.draw.rect(screen, (0, 0, 255), (thirstbar_x, thirstbar_y, HEALTHBAR_WIDTH, CELL_SIZE / 4))
    pygame.draw.rect(screen, (0, 0, 255), (thirstbar_x, thirstbar_y, HEALTHBAR_WIDTH * (agent.thirst / 100), CELL_SIZE / 4))

    # if water_dir != (0, 0):
    #     water_dir_x, water_dir_y = water_dir
    #     water_arrow_x = (agent.pos_x + water_dir_x)
    #     water_arrow_y = (agent.pos_y + water_dir_y)
    #     pygame.draw.rect(screen, (0, 0, 255), (water_arrow_x * CELL_SIZE, water_arrow_y * CELL_SIZE, CELL_SIZE, CELL_SIZE))
    
    # if food_dir != (0, 0):
    #     food_dir_x, food_dir_y = food_dir
    #     food_arrow_x = (agent.pos_x + food_dir_x)
    #     food_arrow_y = (agent.pos_y + food_dir_y)
    #     pygame.draw.rect(screen, (0, 255, 0), (food_arrow_x * CELL_SIZE, food_arrow_y * CELL_SIZE, CELL_SIZE, CELL_SIZE))


    
def take_screenshot(screen, filename):
    pygame.image.save(screen, filename)

def flip_pygame():
    pygame.display.flip()

def quit_pygame():
    pygame.quit()