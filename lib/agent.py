# import threading
import os
from typing import List, Dict, Optional
from enum import Enum
import numpy as np
from numba import njit # type: ignore
from neural_network import FeedForwardNetwork, optimized_choice, weighted_optimized_choice
from .world import Cell, Terrain

class Moves(Enum):
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3

# @njit
def get_obs(grid, observation_space, pos_x, pos_y):
    idx = 0
    for i in range(-1, 1):
        for j in range(-1, 1):
            new_x, new_y = pos_x + i, pos_y + j
            if 0 <= new_x < grid.shape[0] and 0 <= new_y < grid.shape[1]:
                cell_value = grid[new_x, new_y]
            else:
                cell_value = 0  # Assuming 0 is the default terrain value for out-of-bounds
            observation_space[idx] = cell_value / 12
            idx += 1
    return observation_space


class Agent(object):
    def __init__(self, chromosome: Optional[Dict[str, np.ndarray]] = None, pos_x=0, pos_y=0, eval_mode=True):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.runs = []
        self.runs_survived = []
        self.eval_mode = eval_mode

        # self.obs_space = np.zeros(9)
        self.network = FeedForwardNetwork(
            [29, 10, 4]
        )

        self.staticObs = np.zeros(9, dtype=np.float32)

         # If chromosome is set, take it
        if chromosome:
            self.load_chromosome(chromosome)
        self.reset()
    
    def generation_reset(self):
        self.runs = []
        self.runs_survived = []
        self.reset()

    def reset(self):
        self.hunger = 100
        self.thirst = 100
        self.lifespan = 0
        self.is_alive = True
        self.seen = set()

    def finish_run(self):
        self.runs.append(self.last_fitness)
        self.runs_survived.append(self.is_alive)
        self.reset()

    def load_chromosome(self, chromosome: dict):
        num_layers = len(self.network.layers) - 1
        for l in range(num_layers):
            self.network.weights[l] = chromosome['W' + str(l)]
            self.network.biases[l] = chromosome['b' + str(l)]

    @property
    def last_fitness(self):
        return self.lifespan

    @property
    def fitness(self):
        return np.mean(self.runs)
        # return np.median(self.runs)
        if len(self.runs) == 0:
            return 0
        if len(self.runs) == 1:
            return self.runs[0]
    
        self.runs.sort()

        return np.mean(self.runs[:len(self.runs) // 2])
    
    @property
    def chromosome(self):
        pass

    def decode_chromosome(self):
        pass

    def encode_chromosome(self):
        pass
    
    def get_terrain(self, grid: List[List[Cell]]):
        current_cell = grid[self.pos_x][self.pos_y]
        self.seen.add(current_cell.id)
        return current_cell.terrain.value
    
    def get_obs(self, grid: List[List[Cell]], pos_x, pos_y):
        observation_space_landcover = np.zeros(9, dtype=np.float32)
        observation_space_food = np.zeros(9, dtype=np.float32)
        observation_space_water = np.zeros(9, dtype=np.float32)
        # current_cell = grid[pos_x][pos_y]

        # min_distance_to_water = current_cell.terrain.value == Terrain.WATER.value and -100 or float('inf')
        # min_distance_to_food = current_cell.terrain.value == Terrain.GRASS.value and -100 or float('inf')
        # closest_water_direction = (0, 0)
        # closest_food_direction = (0, 0)

        idx = 0
        for i in range(-1, 2):
            for j in range(-1, 2):
                new_x, new_y = pos_x + i, pos_y + j
                if 0 <= new_x < len(grid) and 0 <= new_y < len(grid[0]):
                    cell = grid[new_x][new_y]
                    cell_landcover = cell.terrain.value
                    cell_water_smell = cell.distance_to_water
                    cell_food_smell = cell.distance_to_food
                    # if cell.distance_to_water < min_distance_to_water:
                    #     min_distance_to_water = cell.distance_to_water
                    #     closest_water_direction = (i, j)
                    # if cell.distance_to_food < min_distance_to_food:
                    #     min_distance_to_food = cell.distance_to_food
                    #     closest_food_direction = (i, j)
                else:
                    cell_landcover = 0
                    cell_water_smell = 100
                    cell_food_smell = 100

                observation_space_landcover[idx] = cell_landcover / 12
                observation_space_food[idx] = cell_food_smell / 100
                observation_space_water[idx] = cell_water_smell / 100
                idx += 1
        return (observation_space_landcover, observation_space_food, observation_space_water)
    
    def action(self, grid: List[List[Cell]], move: int):
        if move == Moves.UP.value:
            self.pos_y = max(0, self.pos_y - 1)
        elif move == Moves.DOWN.value:
            self.pos_y = min(len(grid[0]) - 1, self.pos_y + 1)
        elif move == Moves.LEFT.value:
            self.pos_x = max(0, self.pos_x - 1)
        elif move == Moves.RIGHT.value:
            self.pos_x = min(len(grid) - 1, self.pos_x + 1)

    def update(self, grid: List[List[Cell]]):
        if not self.is_alive:
            return (0, 0, self.staticObs)
        
        if (self.get_terrain(grid) == Terrain.WATER.value):
            self.thirst += 20
            self.hunger -= 0.25
        elif (self.get_terrain(grid) == Terrain.GRASS.value):
            self.hunger += 20
            self.thirst -= 0.25
        else:
            self.hunger -= 0.25
            self.thirst -= 0.25

        self.hunger = min(100, self.hunger)
        self.thirst = min(100, self.thirst)
        
        self.lifespan += 1
        if self.hunger <= 0 or self.thirst <= 0:
            self.is_alive = False
        
        (obs, obs_food, obs_water) = self.get_obs(grid, self.pos_x, self.pos_y)
        
        input_vector = np.concatenate(([self.hunger / 100, self.thirst / 100], obs, obs_food, obs_water))
        output = self.network.feed_forward(np.array(input_vector, dtype=np.float32).reshape((29, 1)))
        
        if self.eval_mode:
            move = weighted_optimized_choice(output, 0.75)
        else:
            move = weighted_optimized_choice(output, 0.75)

        self.action(grid, int(move))
        return (obs, obs_food, obs_water)

def save_agent(population_folder: str, individual_name: str, agent: Agent) -> None:
    # Make population folder if it doesnt exist
    if not os.path.exists(population_folder):
        os.makedirs(population_folder)
    
    # Make a directory for the individual
    individual_dir = os.path.join(population_folder, individual_name)
    os.makedirs(individual_dir)

    L = len(agent.network.layers)
    for l in range(1, L):
        w_name = 'W' + str(l)
        b_name = 'b' + str(l)

        weights = agent.network.weights[l - 1]
        bias = agent.network.biases[l - 1]

        np.save(os.path.join(individual_dir, w_name), weights)
        np.save(os.path.join(individual_dir, b_name), bias)

def load_agent(population_folder: str, individual_name: str) -> Agent:
    # Make a directory for the individual
    individual_dir = os.path.join(population_folder, individual_name)
    
    # Initialize a new agent (assuming Agent class has a proper constructor)
    agent = Agent()
    
    # Load weights and biases
    L = len(agent.network.layers)
    weights = []
    biases = []
    
    for l in range(1, L):
        w_name = 'W' + str(l) + '.npy'
        b_name = 'b' + str(l) + '.npy'
        
        w_path = os.path.join(individual_dir, w_name)
        b_path = os.path.join(individual_dir, b_name)
        
        if os.path.exists(w_path) and os.path.exists(b_path):
            weights.append(np.load(w_path))
            biases.append(np.load(b_path))
        else:
            raise FileNotFoundError(f"Could not find weights or biases for layer {l}")

    # Assign the loaded weights and biases to the agent's network
    agent.network.weights = weights # type: ignore
    agent.network.biases = biases # type: ignore
    
    return agent