# import threading
from typing import Tuple
import numpy as np
import random
import csv
import os
import time
from config import Config
from genetic_algorithm.population import Population
from genetic_algorithm.selection import elitism_selection, tournament_selection, roulette_wheel_selection
#from genetic_algorithm.crossover import simulated_binary_crossover as SBX
#from genetic_algorithm.mutation import gaussian_mutation
from .agent import Agent, save_agent
from .world import create_grids, create_grid, debug_grids
from .visualize import init_pygame, draw_agent, draw_grid, quit_pygame, flip_pygame, draw_texts, take_screenshot

def _getMutationRate(current_generation: int) -> float:
    # start off with a high mutation rate, then decrease it over time, from 0.5 to 0.01
    # never go below 0.01
    return max(0.01, 0.5 - 0.01 * current_generation / 100)
    


def _crossover(parent1_weights: np.ndarray, parent2_weights: np.ndarray,
               parent1_bias: np.ndarray, parent2_bias: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    eta = 0.5  # Example value for SBX

    # SBX weights and bias (replace SBX function with your actual crossover function)
    child1_weights, child2_weights = SBX(parent1_weights, parent2_weights, eta)
    child1_bias, child2_bias =  SBX(parent1_bias, parent2_bias, eta)

    return child1_weights, child2_weights, child1_bias, child2_bias

def _mutation(weights: np.ndarray, biases: np.ndarray, mutation_rate: float, scale: float) -> None:
    # Mutate weights
    gaussian_mutation(weights, mutation_rate, scale=scale)
    # Mutate biases
    gaussian_mutation(biases, mutation_rate, scale=scale)

def SBX(parent1: np.ndarray, parent2: np.ndarray, eta: float) -> Tuple[np.ndarray, np.ndarray]:
    # Simulated Binary Crossover (SBX) implementation
    child1 = np.copy(parent1)
    child2 = np.copy(parent2)
    for i in range(parent1.shape[0]):
        if np.random.rand() <= 0.5:
            beta = np.random.rand() * (1 + 2 * eta)
            beta = 1.0 / (beta ** (1 / (eta + 1)))
            child1[i] = 0.5 * ((1 + beta) * parent1[i] + (1 - beta) * parent2[i])
            child2[i] = 0.5 * ((1 - beta) * parent1[i] + (1 + beta) * parent2[i])
    return child1, child2

def gaussian_mutation(param: np.ndarray, mutation_rate: float, scale: float) -> None:
    for i in range(param.shape[0]):
        if np.random.rand() < mutation_rate:
            param[i] += np.random.randn() * scale

class Runner():
    def __init__(self, config: Config, should_save_progress: bool = True, is_root = False):
        self.current_generation = 0
        self.best_fitness = 0
        self.running = False
        self.max_frames = 750000
        self.current_run_frames = 0
        self.best_fitness = 0
        self.best_evaluated_fitness = 0
        self.config = config
        self.data_folder = "data/new_vision_3"
        self.tournament_selection_size = 4
        self.elitism_selection_size = 10
        self.should_save_progress = should_save_progress
        self.is_root = is_root
        self.best_agents = []
        self.best_agent = None
        self.best_ever_fitness = 0

        self._next_gen_size = 0
        if self.config.Selection.selection_type == 'plus':
            self._next_gen_size = config.Selection.num_parents + self.config.Selection.num_offspring
        elif self.config.Selection.selection_type == 'comma':
            self._next_gen_size = config.Selection.num_offspring

        if self.is_root and self.should_save_progress:
            self.evaluation_runner = Runner(config, should_save_progress=False, is_root=False)

        self.grids = create_grids('tiles')
        # self.grids = debug_grids()
        # self.next_grid()

        print(self.config.NeuralNetwork.hidden_layer_architecture)
        agents = []
        for _ in range(100):
            agents.append(Agent())
            
        self.population = Population(agents)

    def next_grid(self):
        # pick random grid
        self.grid = random.choice(self.grids)

    def load_agents(self, agents):
        self.population = Population(agents)

    def all_agents_dead(self):
        return all([not agent.is_alive for agent in self.population.individuals])

    def next_generation(self):
        self.current_generation += 1

        print('START NEXT GEN', self.current_generation)
        print(f'Best fitness of gen: {self.population.fittest_individual.fitness}')

        fittest = Agent()
        fittest.network.weights = self.population.fittest_individual.network.weights
        fittest.network.biases = self.population.fittest_individual.network.biases

        elites = elitism_selection(self.population, self.elitism_selection_size)
        evaluated_fitness = 0
        if self.is_root and self.should_save_progress:
            tournament_agents = []
            for agent in elites + self.best_agents:
                new_agent = Agent(eval_mode=True)
                new_agent.network.weights = agent.network.weights
                new_agent.network.biases = agent.network.biases
                tournament_agents.append(new_agent)
            
            self.evaluation_runner.load_agents(tournament_agents)
            self.evaluation_runner.multiple_runs(50, fixed_frames=8000)
            best_new_candidates = elitism_selection(self.evaluation_runner.population, 3)
            evaluated_fitness = self.evaluation_runner.population.fittest_individual.fitness
            if self.evaluation_runner.population.fittest_individual.fitness > self.best_evaluated_fitness:
                self.best_evaluated_fitness = self.evaluation_runner.population.fittest_individual.fitness
            
            self.best_agents = []
            for agent in best_new_candidates:
                new_agent = Agent()
                new_agent.network.weights = agent.network.weights
                new_agent.network.biases = agent.network.biases
                self.best_agents.append(new_agent)

        if self.should_save_progress:
            if self.population.fittest_individual.fitness > self.best_fitness:
                self.best_fitness = self.population.fittest_individual.fitness
            if self.population.fittest_individual.fitness > self.best_ever_fitness:
                self.best_agent = Agent()
                self.best_agent.network.weights = self.population.fittest_individual.network.weights
                self.best_agent.network.biases = self.population.fittest_individual.network.biases
                save_agent(self.data_folder, "Agent_{}".format(self.current_generation), self.population.fittest_individual)
            elif self.current_generation % 25 == 0:
                save_agent(self.data_folder, "Agent_{}".format(self.current_generation), self.population.fittest_individual)
            save_progress(self.data_folder + "/progress.csv", self, evaluated_fitness)

        self.population.individuals = elites
        random.shuffle(self.population.individuals)
        next_pop = []

        while len(next_pop) < self._next_gen_size:
            p1, p2 = tournament_selection(self.population, 2, self.tournament_selection_size)

            num_layers = len(p1.network.weights)
            c1_params = {}
            c2_params = {}

            # Perform crossover and mutation on each layer
            for l in range(num_layers):
                p1_W_l = p1.network.weights[l]
                p2_W_l = p2.network.weights[l]  
                p1_b_l = p1.network.biases[l]
                p2_b_l = p2.network.biases[l]

                # Crossover
                c1_W_l, c2_W_l, c1_b_l, c2_b_l = _crossover(p1_W_l, p2_W_l, p1_b_l, p2_b_l)

                # Mutation
                _mutation(c1_W_l, c1_b_l, _getMutationRate(self.current_generation), self.config.Mutation.gaussian_mutation_scale)
                _mutation(c2_W_l, c2_b_l, _getMutationRate(self.current_generation), self.config.Mutation.gaussian_mutation_scale)

                # Assign children from crossover/mutation
                c1_params['W' + str(l)] = np.clip(c1_W_l, -1, 1)
                c2_params['W' + str(l)] = np.clip(c2_W_l, -1, 1)
                c1_params['b' + str(l)] = np.clip(c1_b_l, -1, 1)
                c2_params['b' + str(l)] = np.clip(c2_b_l, -1, 1)

            c1 = Agent(chromosome=c1_params)
            c2 = Agent(chromosome=c2_params)

            next_pop.extend([c1, c2])
        
        next_pop.extend([fittest])
        if self.best_agent is not None:
            next_pop.extend([self.best_agent])
        if self.current_generation < 50:
            for _ in range(5):
                next_pop.append(Agent())
        next_pop.extend(self.best_agents)
        
        self.population = Population(next_pop)

    def multiple_runs(self, num_runs: int, fixed_frames: int = -1):
        for _ in range(num_runs):
            self.run(should_visualize=False, fixed_frames=fixed_frames)

    def prepare_run(self):
        self.next_grid()
        self.current_run_frames = 0
        if self.should_save_progress:
            pos_x = np.random.randint(0, len(self.grid))
            pos_y = np.random.randint(0, len(self.grid[0]))
            for agent in self.population.individuals:
                agent.pos_x = pos_x
                agent.pos_y = pos_y
        else:
            for agent in self.population.individuals:
                agent.pos_x = np.random.randint(0, len(self.grid))
                agent.pos_y = np.random.randint(0, len(self.grid[0]))


    def run(self, should_visualize: bool = False, fixed_frames: int = -1):
        self.prepare_run()

        self.running = True
        screen = None
        if should_visualize:
            screen = init_pygame()

        while self.running:
            if should_visualize:
                draw_grid(screen, self.grid)
            
            for agent in self.population.individuals:
                (water_dir, food_dir, obs) = agent.update(self.grid)
                # print(water_dir)
                if should_visualize:
                    draw_agent(screen, agent, water_dir, food_dir, obs)

            if should_visualize:
                draw_texts(screen, self.current_run_frames, sum([1 for agent in self.population.individuals if agent.is_alive]))
                flip_pygame()

            if self.all_agents_dead() or self.current_run_frames > self.max_frames:
                self.running = False
            self.current_run_frames += 1
            if fixed_frames > 0:
                fixed_frames -= 1
                if fixed_frames == 0:
                    if (should_visualize):
                        take_screenshot(screen, 'screenshot.png')
                    self.running = False
        
        for agent in self.population.individuals:
            agent.finish_run()

        if should_visualize:
            quit_pygame()

def save_progress(file_path, runner: Runner, evaluated_fitness):
    file_exists = os.path.isfile(file_path)
    
    with open(file_path, 'a', newline='') as csvfile:
        all_fieldnames = set()

        # Collect generation data
        generation_data = {
            'generation': runner.current_generation,
            'best_fitness': runner.best_fitness,
            'generation_best_fitness': runner.population.fittest_individual.fitness,
            'generation_average_fitness': runner.population.average_fitness,
            'evaluated_fitness': evaluated_fitness,
            'best_evaluated_fitness': runner.best_evaluated_fitness,
            'survival_rate': runner.population.average_survival_rate,
        }

        agents_data = {}
        for i, agent in enumerate(runner.population.individuals):
            agents_data[f'agent_{i}_fitness'] = agent.fitness
            agents_data[f'agent_{i}_survival_rate'] = sum(agent.runs_survived)
            for run_index, fitness in enumerate(agent.runs):
                agents_data[f'agent_{i}_run_{run_index}_fitness'] = fitness
                agents_data[f'agent_{i}_run_{run_index}_survived'] = agent.runs_survived[run_index]
        
        agent_fieldnames = list(agents_data.keys())
        agent_fieldnames.sort()
        fieldnames = list(generation_data.keys()) + agent_fieldnames

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        if not file_exists:
            writer.writeheader()  # file doesn't exist yet, write a header

        # Merge generation data with agents data
        row = {**generation_data, **agents_data}
        writer.writerow(row)