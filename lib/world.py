# import threading
import os
from typing import List, Any
from PIL import Image
from enum import Enum
import numpy as np

class Moves(Enum):
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3

class Terrain(Enum):
    EMPTY = 0
    WATER = 1
    GRASS = 2
    TREES = 3
    CROPS = 4
    SHRUB = 5
    BUILT = 6
    BARE = 7
    SNOW = 8
    FLOODED_VEGETATION = 9
    ME = 10
    ENEMY = 11

color_to_terrain = {
    (65, 155, 223): Terrain.WATER,
    (57, 125, 73): Terrain.TREES,
    (136, 176, 83): Terrain.GRASS,
    (122, 135, 198): Terrain.FLOODED_VEGETATION,
    (228, 150, 53): Terrain.CROPS,
    (223, 195, 90): Terrain.SHRUB,
    (196, 40, 27): Terrain.BUILT,
    (165, 155, 143): Terrain.BARE,
    (179, 159, 225): Terrain.SNOW,
    (0, 0, 0): Terrain.EMPTY
}
terrain_to_color = {v: k for k, v in color_to_terrain.items()}

class Cell(object):
    def __init__(self, terrain: Terrain = Terrain.EMPTY):
        self.id = 0
        self.terrain = terrain
        self.distance_to_water = 0.0
        self.distance_to_food = 0.0

def cell_from_pixel(r, g, b) -> Cell:
    terrain = color_to_terrain.get((r, g, b), Terrain.EMPTY)
    return Cell(
        terrain=terrain
    )

def populate_distances(grid: List[List[Cell]]):
    water_positions = []
    food_positions = []
    
    # Collect positions of water and grass cells
    for y, row in enumerate(grid):
        for x, cell in enumerate(row):
            if cell.terrain == Terrain.WATER:
                water_positions.append((x, y))
            elif cell.terrain == Terrain.GRASS:
                food_positions.append((x, y))
    
    # Update distances to water/food
    for y, row in enumerate(grid):
        for x, cell in enumerate(row):
            if (cell.terrain == Terrain.WATER):
                cell.distance_to_water = 0
            else:
                cell.distance_to_water = min(
                    [abs(x - wx) + abs(y - wy) for wx, wy in water_positions]
                    or [float('inf')]
                ) + 1
            if (cell.terrain == Terrain.GRASS):
                cell.distance_to_food = 0
            else:
                cell.distance_to_food = min(
                    [abs(x - fx) + abs(y - fy) for fx, fy in food_positions]
                    or [float('inf')]
                ) + 1

    # loop through and check so that no water is 1 away from food
    for y, row in enumerate(grid):
        for x, cell in enumerate(row):
            if (cell.terrain == Terrain.GRASS and cell.distance_to_water == 1):
                print("Found grass 1 away from water, " + str(x) + ", " + str(y))
        

def create_grid(image_path):
    image = Image.open(image_path)
    image = image.convert('RGB')
    pixel_data = np.array(image)

    print("Creating grid from pixel data")
    grid = []
    cell_id = 0
    for y in range(pixel_data.shape[0]):
        row = []
        for x in range(pixel_data.shape[1]):
            # Get the RGB value of the pixel
            r, g, b = pixel_data[x, y]
            # Create a Cell based on RGB value
            # You can define how you want to map RGB values to Cell attributes
            cell = cell_from_pixel(r, g, b)
            cell.id = cell_id
            cell_id += 1
            row.append(cell)
        grid.append(row)

    populate_distances(grid)
    
    return grid

def debug_grids():
    grids = []
    filepath = 'simple.png'
    grid = create_grid(filepath)
    grids.append(grid)
    # Flip the grid horizontally
    grid = np.flip(grid, 0)
    grids.append(grid)
    # Flip the grid vertically
    grid = np.flip(grid, 1)
    grids.append(grid)
    # Rotate the grid 90 degrees
    grid = np.rot90(grid)
    grids.append(grid)
    # Flip the rotated grid horizontally
    grid = np.flip(grid, 0)
    grids.append(grid)
    # Flip the rotated grid vertically
    grid = np.flip(grid, 1)
    grids.append(grid)
    # Rotate the grid 90 degrees
    grid = np.rot90(grid)
    grids.append(grid)
    # Flip the rotated grid horizontally
    grid = np.flip(grid, 0)
    grids.append(grid)
    return grids

def create_grids(tiles_folder):
    grids = []
    
    # Loop through all files in the specified directory
    for root, _, files in os.walk(tiles_folder):
        for file in files:
            if file.endswith('.png'):
                filepath = os.path.join(root, file)
                grid = create_grid(filepath)
                grids.append(grid)
                # Flip the grid horizontally
                grid = np.flip(grid, 0)
                grids.append(grid)
                # Flip the grid vertically
                grid = np.flip(grid, 1)
                grids.append(grid)
                # Rotate the grid 90 degrees
                grid = np.rot90(grid)
                grids.append(grid)
                # Flip the rotated grid horizontally
                grid = np.flip(grid, 0)
                grids.append(grid)
                # Flip the rotated grid vertically
                grid = np.flip(grid, 1)
                grids.append(grid)
                # Rotate the grid 90 degrees
                grid = np.rot90(grid)
                grids.append(grid)
                # Flip the rotated grid horizontally
                grid = np.flip(grid, 0)
                grids.append(grid)
                
    return grids