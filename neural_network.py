import numpy as np
from typing import List, Callable, NewType, Optional
from numba import njit, jit # type: ignore

@njit
def relu(Z):
    return np.maximum(0, Z)

@njit
def sigmoid(Z):
    return 1 / (1 + np.exp(-Z))

@njit
def softmax(X):
    exp_x = np.exp(X - np.max(X))
    return exp_x / np.sum(exp_x)

@njit
def optimized_choice(probabilities):
    cumulative_probabilities = np.cumsum(probabilities)
    random_value = np.random.rand()
    return np.searchsorted(cumulative_probabilities, random_value)

@njit
def weighted_optimized_choice(probabilities, bias_weight = 0.5):
    max_index = np.argmax(probabilities)
    max_probability = probabilities[max_index]
    
    # Create a biased probability distribution
    biased_probabilities = probabilities * (1 - bias_weight)
    biased_probabilities[max_index] += max_probability * bias_weight
    
    # Normalize the biased probabilities
    biased_probabilities /= biased_probabilities.sum()
    
    # Compute the cumulative probabilities
    cumulative_probabilities = np.cumsum(biased_probabilities)
    
    # Generate a random value
    random_value = np.random.rand()
    
    # Return the index based on the random value
    return np.searchsorted(cumulative_probabilities, random_value)

class FeedForwardNetwork:
    def __init__(self, seed: Optional[int] = None):
        # self.layers = layer_nodes
        self.rand = np.random.RandomState(seed)

        # hunger + thirst + 3x3 vision + 3x3 food smell + 3x3 water smell
        # 2x10 hidden layers
        # 4 output nodes
        self.layers = [29, 10, 10, 4]
        self.weights = np.array([self.rand.uniform(-1, 1, size=(self.layers[i + 1], self.layers[i])) for i in range(len(self.layers) - 1)], dtype=object)
        self.biases = np.array([self.rand.uniform(-1, 1, size=(self.layers[i + 1], 1)) for i in range(len(self.layers) - 1)], dtype=object)

    def feed_forward(self, X: np.ndarray) -> np.ndarray:
        A = X
        for i in range(2):
            Z = np.dot(self.weights[i], A) + self.biases[i]
            A = relu(Z)

        Z = np.dot(self.weights[-1], A) + self.biases[-1]
        output = sigmoid(Z)
        
        return softmax(output).flatten()
