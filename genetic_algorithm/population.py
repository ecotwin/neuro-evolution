import numpy as np
from typing import List
from lib.agent import Agent
    

class Population(object):
    def __init__(self, individuals: List[Agent]):
        self.individuals = individuals
        for agent in self.individuals:
            agent.generation_reset()

    @property
    def num_individuals(self) -> int:
        return len(self.individuals)

    @property
    def average_fitness(self):
        return sum(individual.fitness for individual in self.individuals) / float(self.num_individuals)
    
    @property
    def average_survival_rate(self):
        return sum(sum(individual.runs_survived) for individual in self.individuals) / float(self.num_individuals)

    @property
    def fittest_individual(self) -> Agent:
        return max(self.individuals, key = lambda individual: individual.fitness)

    def get_fitness_std(self):
        return np.std(np.array([individual.fitness for individual in self.individuals]))