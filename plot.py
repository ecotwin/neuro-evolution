import csv
import os
import matplotlib.pyplot as plt
import math
import numpy as np
import pandas as pd

def read_csv(file_path, cutoff=100):
    generations = []
    best_fitness = []
    generation_best_fitness = []
    generation_average_fitness = []
    evaluated_fitness = []
    best_evaluated_fitness = []
    survival_rate = []
    agent_fitnesses = []
    
    with open(file_path, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        num_agents = 100

        for row in reader:
            if cutoff > 0 and int(row['generation']) > cutoff:
                break
            generations.append(int(row['generation']))
            best_fitness.append(float(row['best_fitness']))
            generation_best_fitness.append(float(row['generation_best_fitness']))
            generation_average_fitness.append(float(row['generation_average_fitness']))
            evaluated_fitness.append(float(row['evaluated_fitness']))
            best_evaluated_fitness.append(float(row['best_evaluated_fitness']))
            survival_rate.append(float(row['survival_rate']))

            # Collect agent fitness values for each generation

            # gen_fitness = []
            # for i in range(num_agents):
            #     # check if key exists
            #     if f'agent_{i}_fitness' not in row:
            #         continue
                
            #     value = row[f'agent_{i}_fitness']
            #     if value == None or value == '':
            #         continue

            #     agent_data = float(value)
            #     gen_fitness.append(agent_data)
            # agent_fitnesses.append(gen_fitness)
    
    return generations, best_fitness, generation_best_fitness, generation_average_fitness, evaluated_fitness, best_evaluated_fitness, survival_rate, agent_fitnesses

def find_global_max(folders):
    global_max = -float('inf')
    for folder in folders:
        file_path = os.path.join(folder, 'progress.csv')
        if os.path.exists(file_path):
            generations, best_fitness, generation_best_fitness, generation_average_fitness, evaluated_fitness, best_evaluated_fitness, survival_rate, agent_fitness = read_csv(file_path)
            max_value = max(max(best_fitness), max(generation_best_fitness), max(generation_average_fitness), max(evaluated_fitness), max(best_evaluated_fitness))
            # max_value = max(generation_average_fitness)
            if max_value > global_max:
                global_max = max_value
        else:
            print(f"File {file_path} not found")
    return global_max

def smooth_data(x, y, window_size=1):
    df = pd.DataFrame({'x': x, 'y': y})
    df['y'] = df['y'].rolling(window=window_size, min_periods=1).mean()
    return df['x'].tolist(), df['y'].tolist()

def plot_single_chart(folder):
    file_path = os.path.join(folder, 'progress.csv')
    generations, best_fitness, generation_best_fitness, generation_average_fitness, evaluated_fitness, best_evaluated_fitness, survival_rate, agent_fitnesses = read_csv(file_path)

    print(np.mean(generation_best_fitness))

    generations_smooth, generation_best_fitness_smooth = smooth_data(generations, generation_best_fitness)
    _, generation_average_fitness_smooth = smooth_data(generations, generation_average_fitness)

    plt.figure(figsize=(10, 6), dpi=300)
    plt.style.use('seaborn-v0_8-whitegrid')

    # plt.plot(generations_smooth, best_fitness, label='Best Fitness', alpha=0.6)
    plt.plot(generations_smooth, generation_best_fitness_smooth, label='Gen Best Fitness', alpha=0.6)
    # plt.plot(generations_smooth, generation_average_fitness_smooth, label='Gen Avg Fitness', alpha=0.6)

    # static line at 808
    plt.axhline(y=808, color='r', linestyle='--', label='Random agent fitness')


    plt.title('Fitness over time', fontsize=16, fontweight='bold')
    plt.xlabel('Generation', fontsize=14)
    plt.ylabel('Fitness', fontsize=14)
    plt.legend(fontsize=12)
    plt.grid(True, which='both', linestyle='--', linewidth=0.5)
    plt.tight_layout()

    plt.savefig(os.path.join(folder, 'fitness_progress.png'), format='png', bbox_inches='tight')
    plt.show()


def plot_data(folders, global_max):
    num_folders = len(folders)
    cols = 3
    rows = math.ceil(num_folders / cols)
    fig, axs = plt.subplots(rows, cols, figsize=(20, 5 * rows))
    
    for idx, folder in enumerate(folders):
        file_path = os.path.join(folder, 'progress.csv')
        if os.path.exists(file_path):
            generations, best_fitness, generation_best_fitness, generation_average_fitness, evaluated_fitness, best_evaluated_fitness, survival_rate, agent_fitnesses = read_csv(file_path)
            
            generations_smooth, best_fitness_smooth = smooth_data(generations, best_fitness)
            _, generation_best_fitness_smooth = smooth_data(generations, generation_best_fitness)
            _, generation_average_fitness_smooth = smooth_data(generations, generation_average_fitness)
            _, evaluated_fitness_smooth = smooth_data(generations, evaluated_fitness)
            _, best_evaluated_fitness_smooth = smooth_data(generations, best_evaluated_fitness)
            _, survival_rate_smooth = smooth_data(generations, survival_rate)
            
            row, col = divmod(idx, cols)
            ax = axs[row, col] if rows > 1 else axs[col]
            ax.plot(generations_smooth, best_fitness_smooth, label='Best Fitness', alpha=0.6)
            ax.plot(generations_smooth, generation_best_fitness_smooth, label='Gen Best Fitness', alpha=0.6)
            ax.plot(generations_smooth, generation_average_fitness_smooth, label='Gen Avg Fitness', alpha=0.6)
            ax.plot(generations_smooth, evaluated_fitness_smooth, label='Evaluated Fitness', alpha=0.6)
            ax.plot(generations_smooth, best_evaluated_fitness_smooth, label='Best Evaluated Fitness', alpha=0.6)
            # ax.plot(generations_smooth, survival_rate_smooth, label='Survival Rate', alpha=0.6)

            # show band of min/max for each generation of agent fitness
            # agent_fitness = list(zip(*agent_fitnesses)) 
            # for agent_data in agent_fitness:
            #     # plot them as dots
            #     ax.plot(generations, agent_data, 'o', markersize=1, alpha=0.1)
                
            
            # top_10 = sorted(agent_fitness, reverse=True)[:10]
            # bottom_50 = sorted(agent_fitness)[:50]
            # ax.fill_between(generations, min(top_10), max(top_10), color='green', alpha=0.3, label='Top 10 Fitness')
            # ax.fill_between(generations, min(bottom_50), max(bottom_50), color='red', alpha=0.3, label='Bottom 50 Fitness')
            # ax.set_ylim(0, global_max * 1.1)
            # take last part of folder name
            name = folder.split('/')[-1]
            ax.set_title(f'{name}')
            ax.set_xlabel('Generation')
            ax.set_ylabel('Fitness')
            ax.legend(fontsize=4)
            ax.grid(True)
        else:
            print(f"File {file_path} not found")
    
    # Remove empty subplots
    if num_folders % cols != 0:
        for idx in range(num_folders, rows * cols):
            row, col = divmod(idx, cols)
            fig.delaxes(axs[row, col] if rows > 1 else axs[col])
    
    plt.tight_layout()
    plt.show()

# Example usage
# all_folders = [f'data/tournament_fixed_50_{i}' for i in range(1, 7)]  # Replace with the actual folder names
# folders = [f'data/knowledge_lab_3/bottom50_tournament_{i}' for i in range(1, 6)]  # Replace with the actual folder names
# folders_2 = [f'data/knowledge_lab_3/discard_best_bottom50_tournament_{i}' for i in range(1, 6)]  # Replace with the actual folder names
# folders_3 = [f'data/knowledge_lab_3/median_tournament_{i}' for i in range(1, 6)]  # Replace with the actual folder names
# all_folders = folders + folders_2 + folders_3

# folders = [f'data/harder_maps_{i}' for i in range(1, 6)]  # Replace with the actual folder names
# folders_2 = [f'data/harder_maps_b50_{i}' for i in range(1, 6)]  # Replace with the actual folder names
# folders_3 = [f'data/harder_maps_b50_easier_{i}' for i in range(1, 6)]  # Replace with the actual folder names

folders = [f'data/new_vision_{i}' for i in range(1, 4)]  # Replace with the actual folder names
# folders_2 = [f'data/after_midsummer_kl/argmax_tournament_size4_{i}' for i in range(1, 6)]  # Replace with the actual folder names
# folders_3 = [f'data/after_midsummer_kl/argmax_tournament_elite_25_size4_{i}' for i in range(1, 6)]  # Replace with the actual folder names
all_folders = folders

# plot_single_chart('data/after_midsummer_kl/weighted_tournament_size4_2')

# all_folders = [
#     'data/argmax_tournament_size2_1'
# ]

global_max = find_global_max(all_folders)
plot_data(all_folders, global_max)

# plot_data(['data/test_run_1'])